import java.util.Scanner;

public class Main {
    private static Scanner input = new Scanner(System.in);

    public static void menu() {
        System.out.println("\nEnter:");
        System.out.println("\ta. Create Bank");
        System.out.println("\tb. Add a branch to a Bank");
        System.out.println("\tc. Add a customer to a Branch");
        System.out.println("\td. Make a transaction with a customer");
        System.out.println("\te. Display Banks, Branches, Customers, and Transactions.");
        System.out.println("\tq. Quit Application.");
        System.out.print("\nSelection -> ");

    }

    public static void main(String[] args)
    {
        System.out.println("Welcome to Frank's Banking Application.");

        MenuOptions menuOptions = MenuOptions.z;

        while (menuOptions != MenuOptions.q) {
            try {
                menu();

                menuOptions = MenuOptions.valueOf(input.nextLine());

                switch (menuOptions) {
                    case a:
                        System.out.println("Bank has been created");
                        break;

                    case b:
                        System.out.println("Branch has been added");
                        break;

                    case c:
                        System.out.println("Customer has been added");
                        break;

                    case d:
                        System.out.println("Transaction has been done");
                        break;

                    case e:
                        System.out.println("Bank "+"Lviv"+" in branches Lichakivska 55, Custumer Ivanov P. P. withdraw amount many");
                        break;

                    case q:
                        System.out.println("Goodbye.");
                        break;

                    default:
                        System.out.println("Selection out of range. Try again");
                }
            } catch (IllegalArgumentException e) {
                System.out.println("Selection out of range. Try again:");
            }

        }
        System.out.println("---------------------------------------------------------------------------------");
        BinaryTree t = new BinaryTree();
        System.out.println("Binary tree :");
        t.insert(5);
        t.insert(3);
        t.insert(8);
        t.insert(1);
        t.insert(4);
        t.insert(7);
        t.insert(9);
        t.insert(0);
        t.insert(2);
        t.insert(6);
        t.insert(10);

        t.print(t.root, 0, 0);
    }
}
