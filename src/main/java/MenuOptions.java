public enum MenuOptions {
    z("Continue"), a("Create Bank"), b("Add Branch"), c("Add Customer"),
    d("Make Transaction"), e("Display Information"), q("Quit");

    // field
    private String meaning;

    // constructor
    MenuOptions(String meaning)
    {
        this.meaning = meaning;
    }

    // getters
    public String getMeaning()
    {
        return meaning;
    }
}
